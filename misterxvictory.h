#ifndef MISTERXVICTORY_H
#define MISTERXVICTORY_H

#include <QWidget>
#include "misterxengine.h"

namespace Ui {
    class MisterxVictory;
}

class MisterxVictory : public QWidget {
    Q_OBJECT
public:
    MisterxVictory(MisterxEngine *engine,QWidget *parent = 0);
    ~MisterxVictory();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::MisterxVictory *ui;
};

#endif // MISTERXVICTORY_H
