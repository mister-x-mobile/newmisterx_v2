#include "misterxengine.h"
#include <QTimer>
#include <QPointF>
#include <iostream>
#include <QDebug>
#include <QHash>
#include <QFile>
#include <QString>
#include <QList>
#include <QStringList>


MisterxEngine::MisterxEngine(QObject *parent)
:QObject(parent)
{
    localUser= new MisterxUser(this);
    updateTimer = new QTimer();
    updateTimer->setInterval(3000);
    qsrand(QTime::currentTime().msec());
    //updateTimer->stop();
    step=0;
    misterx = FALSE;
    connect(updateTimer,SIGNAL(timeout()), this, SLOT(timerEnd()));
}

MisterxEngine::~MisterxEngine()
{


}

void MisterxEngine::startGame()
{
    this->initSimulation();
    this->updateTimer->start();
    emit gameStarted();
}

void MisterxEngine::stopGame()
{
    this->updateTimer->stop();
    emit gameStoped();
}


void MisterxEngine::initSimulation()
{
    MisterxUser *user;
    QList<QString> *userCoords;


    user = new MisterxUser();
    user->jid = "misterx@demo";
    user->role = MisterxUser::Misterx;
    user->itsMe = this->misterx;
    user->lat = 46.52074673493533;
    user->lng = 6.630168384552011;
    this->userList.append(user);

    userCoords = new QList<QString>;
    userCoords->append("46.52074673493533,6.630168384552011");
    userCoords->append("46.520872236013766,6.633859104156503");
    userCoords->append("46.52104941351328,6.637463993072519");
    userCoords->append("46.519100429240545,6.643901294708261");
    userCoords->append("46.51774200491906,6.646562046051034");
    userCoords->append("46.519100429240545,6.649222797393808");
    userCoords->append("46.52069505793643,6.647935337066659");
    userCoords->append("46.52175811773356,6.646562046051034");
    userCoords->append("46.52406134260498,6.647592014312753");
    userCoords->append("46.52606920256745,6.649566120147714");
    userCoords->append("46.52618730966589,6.651883548736581");
    userCoords->append("46.523825118906885,6.653771823883066");
    userCoords->append("46.52270304231367,6.654715961456308");
    userCoords->append("46.521285649280514,6.6549734535217375");
    userCoords->append("46.52045881960127,6.656861728668222");
    userCoords->append("46.519986339849396,6.658750003814706");
    userCoords->append("46.51921855148962,6.656175083160409");
    userCoords->append("46.518746060952516,6.653256839752206");
    userCoords->append("46.51667886654355,6.652312702178964");
    userCoords->append("46.51461159349165,6.652999347686777");
    userCoords->append("46.51526131635359,6.649995273590097");
    userCoords->append("46.515202250959874,6.646647876739511");
    userCoords->append("46.51437532870682,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    simulationFiles[user]=userCoords;

    user = new MisterxUser();
    user->jid = "self@demo";
    user->role = MisterxUser::Player;
    user->itsMe = !this->misterx;
    user->lat = 46.52058432134472;
    user->lng = 6.630136198043832;
    this->userList.append(user);

    userCoords = new QList<QString>;
    userCoords->append("46.52058432134472,6.630136198043832");
    userCoords->append("46.52058432134472,6.630136198043832");
    userCoords->append("46.52058432134472,6.630136198043832");
    userCoords->append("46.52058432134472,6.630136198043832");
    userCoords->append("46.52058432134472,6.630136198043832");
    userCoords->append("46.52058432134472,6.630136198043832");
    userCoords->append("46.52058432134472,6.630136198043832");
    userCoords->append("46.520414524799094,6.629267162323007");
    userCoords->append("46.520148755227574,6.628795093536386");
    userCoords->append("46.51955815152538,6.630425876617441");
    userCoords->append("46.51929237776494,6.631970829010019");
    userCoords->append("46.51879035711495,6.631584590911874");
    userCoords->append("46.51834739386601,6.633515781402597");
    userCoords->append("46.51749098801176,6.633987850189218");
    userCoords->append("46.51687082362236,6.635404056549081");
    userCoords->append("46.51660503671733,6.637378162384042");
    userCoords->append("46.51660503671733,6.639223522186288");
    userCoords->append("46.516723164391166,6.64094013595582");
    userCoords->append("46.51563047360994,6.640553897857675");
    userCoords->append("46.515364680638164,6.640983051300058");
    userCoords->append("46.51557140861744,6.6419271888733");
    userCoords->append("46.51480355787302,6.643343395233163");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    userCoords->append("46.514419628432236,6.644158786773691");
    simulationFiles[user]=userCoords;

    user = new MisterxUser();
    user->jid = "player1@demo";
    user->role = MisterxUser::Player;
    user->itsMe = FALSE;
    user->lat = 46.520839015168235;
    user->lng = 6.630216664314279;
    this->userList.append(user);

    userCoords = new QList<QString>;
    userCoords->append("46.520839015168235,6.630216664314279");
    userCoords->append("46.520839015168235,6.630216664314279");
    userCoords->append("46.520839015168235,6.630216664314279");
    userCoords->append("46.520839015168235,6.630216664314279");
    userCoords->append("46.520839015168235,6.630216664314279");
    userCoords->append("46.520839015168235,6.630216664314279");
    userCoords->append("46.520839015168235,6.630216664314279");
    userCoords->append("46.5206249247879,6.632024473190317");
    userCoords->append("46.52077257341636,6.63369817161561");
    userCoords->append("46.5209497512409,6.635715192794809");
    userCoords->append("46.52168798595726,6.6370455684661955");
    userCoords->append("46.522898669177216,6.637217229843149");
    userCoords->append("46.52434554788048,6.637217229843149");
    userCoords->append("46.52534948188608,6.637775129318246");
    userCoords->append("46.525733334102796,6.639405912399301");
    userCoords->append("46.526353397338646,6.640822118759164");
    userCoords->append("46.52673724246158,6.642109579086313");
    userCoords->append("46.52673724246158,6.6439120235443205");
    userCoords->append("46.52673724246158,6.6439120235443205");
    userCoords->append("46.52555617187826,6.645113653182992");
    userCoords->append("46.52440460333556,6.645456975936899");
    userCoords->append("46.523430180117415,6.645628637313852");
    userCoords->append("46.52248526818423,6.646014875411996");
    userCoords->append("46.52151081054016,6.646615690231332");
    userCoords->append("46.520300096392916,6.6458861293792815");
    userCoords->append("46.5192074775397,6.64541406059266");
    userCoords->append("46.51808530557664,6.644384092330942");
    userCoords->append("46.51764233657998,6.643525785446176");
    userCoords->append("46.516667792076035,6.6442124309539885");
    userCoords->append("46.51581135975005,6.6445128383636565");
    userCoords->append("46.515870424481726,6.642796224594125");
    userCoords->append("46.51589995682349,6.6414658489227385");
    userCoords->append("46.51504351239611,6.64266747856141");
    userCoords->append("46.514305187401085,6.6442124309539885");
    userCoords->append("46.514305187401085,6.6442124309539885");
    userCoords->append("46.514305187401085,6.6442124309539885");
    userCoords->append("46.514305187401085,6.6442124309539885");
    simulationFiles[user]=userCoords;
}

void MisterxEngine::timerEnd(void)
{
    //TODO: GPS daten auslesen und in local user speichern.


    qDebug() << "Game updated" << endl;
    foreach(MisterxUser *user, this->userList) {

        QString coord= simulationFiles[user]->at(step);
        QStringList temp = coord.split(",",QString::SkipEmptyParts);
        user->lat = temp.at(0).toDouble();
        user->lng = temp.at(1).toDouble();
        emit gameUpdated(user);
    }
    if(step>=36){
        qDebug() << "Game Ended" << endl;
        this->stopGame();
    }
    else step++;
    emit timerEnded();
}

