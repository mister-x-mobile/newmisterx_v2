#ifndef MISTERXENGINE_H
#define MISTERXENGINE_H

#include "misterxuser.h"
#include <QTimer>
#include <QPointF>
#include <QList>
#include <QHash>

class MisterxEngine : public QObject
{

        Q_OBJECT

public:
        MisterxEngine(QObject *parent=0);
        ~MisterxEngine();
        MisterxUser *localUser;
        QList<MisterxUser*> userList;
        bool misterx;
        QTimer* updateTimer;
        QList<QString> playerList;
        QHash<QString,MisterxUser*> playerhash;
        QHash<MisterxUser*, QList<QString>*> simulationFiles;

        void initSimulation();
public slots:
        void startGame();
        void stopGame();

signals:
        void gameStarted();
        void gameStoped();
        void gameUpdated(MisterxUser*);
        void timerEnded();

private slots:
        void timerEnd(void);
private:
        int step;

};

#endif // MISTERXENGINE_H
