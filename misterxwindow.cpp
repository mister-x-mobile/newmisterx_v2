#include "misterxwindow.h"
#include "ui_misterxwindow.h"

#include <QTimer>

MisterxWindow::MisterxWindow(MisterxEngine *engine, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MisterxWindow)
{
    this->engine = engine;
    ui->setupUi(this);




////////////////////////////painter//////////////////
    angle = 0;
    paintTimer = new QTimer(this);
    connect(paintTimer, SIGNAL(timeout()), this, SLOT(update()));
    paintTimer->start(50);
    this->scene = new QGraphicsScene;

    ui->stateImage->setBackgroundBrush(Qt::black);
    ui->stateImage->setRenderHint(QPainter::Antialiasing);
    ui->stateImage->setScene(scene);

    state=new QGraphicsRectItem(-15,-20,30,40);
    state->setBrush(Qt::white);

    this->scene->addItem(state);
}

MisterxWindow::~MisterxWindow()
{
    delete ui;
}

void MisterxWindow::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MisterxWindow::on_hostButton_clicked()
{
    engine->misterx=TRUE;
    emit MisterxWindow::hostClicked();
}



void MisterxWindow::on_joinButton_clicked()
{
    engine->misterx=FALSE;
    emit MisterxWindow::joinClicked();
}

void MisterxWindow::on_settingsButton_clicked()
{
    emit MisterxWindow::settingsClicked();
}

void MisterxWindow::paintEvent(QPaintEvent* event){
    state->rotate(3);
}

