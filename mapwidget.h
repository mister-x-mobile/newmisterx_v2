#ifndef MAPWIDGET_H
#define MAPWIDGET_H

#include <QWidget>
#include <QUrl>
#include <QRect>
#include "misterxengine.h"

#define MARKER_MISTERX 1
#define MARKER_USER 2
#define MARKER_OTHERHUNTERS 3

namespace Ui {
    class MapWidget;
}

class MapWidget : public QWidget {
    Q_OBJECT
public:
    MapWidget(MisterxEngine *engine, QWidget *parent = 0);
    ~MapWidget();

    int zoomLevel;
    double definedAreaCenterLat;
    double definedAreaCenterLng;

protected:
    void changeEvent(QEvent *e);

private:
    Ui::MapWidget *ui;
    MisterxEngine *engine; 

public slots:
    void defineArea();

private slots:
    void viewArea();
    void viewUser();
    void setMarker(QString markerID, double lat, double lng, int modus);
    void setMarkerPosition(QString markerID, double newLat, double newLng);
    void initMarker();
    void updateMarker(MisterxUser* user);
    void navigateRight();
    void navigateLeft();
    void navigateUp();
    void navigateDown();
    void zoomPlus();
    void zoomMinus();
    //setMarkers( QList<QPoint> pointList);
    //setFocusPlayer(int nr);

signals:
    void areaDefined();

};

#endif // MAPWIDGET_H
