#ifndef MisterxSETTINGSFORM_H
#define MisterxSETTINGSFORM_H

#include <QWidget>

namespace Ui {
    class MisterxSettingsForm;
}

class MisterxSettingsForm : public QWidget {
    Q_OBJECT
public:
    MisterxSettingsForm(QWidget *parent = 0);
    ~MisterxSettingsForm();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::MisterxSettingsForm *ui;

private slots:
    void on_cancelButton_clicked();
    void on_saveButton_clicked();

signals:
    void cancelClicked();
};

#endif // MisterxSETTINGSFORM_H
