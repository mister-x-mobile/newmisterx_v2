# -------------------------------------------------
# Project created by QtCreator 2009-12-16T11:18:35
# -------------------------------------------------
QT += network \
    webkit \
    xml
TARGET = MisterX
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    mapwidget.cpp \
    misterxengine.cpp \
    misterxuser.cpp \
    misterxsettingsform.cpp \
    misterxwindow.cpp \
    connectwidget.cpp \
    misterxvictory.cpp
HEADERS += mainwindow.h \
    mapwidget.h \
    misterxengine.h \
    misterxuser.h \
    misterxsettingsform.h \
    misterxwindow.h \
    sym_iap_util.h \
    connectwidget.h \
    misterxvictory.h
FORMS += mainwindow.ui \
    mapwidget.ui \
    misterxsettingsform.ui \
    misterxwindow.ui \
    connectwidget.ui \
    misterxvictory.ui
OTHER_FILES += 
symbian { 
    HEADERS += sym_iap_util.h
    INCLUDEPATH += $$APP_LAYER_SYSTEMINCLUDE
    TARGET.CAPABILITY = "NetworkServices ReadUserData WriteUserData"
    LIBS += -lesock \
        -lcommdb \
        -linsock \
        \ \
        \ \
        \ \
        \ \
        \ \
        \ \
        \ \
        \ \
        \ \
        \ \
        \ \ # For IAP selection
}
    
    # include(qxmpp/source/source.pro)
    # QXmpp Files
    HEADERS += qxmpp/source/QXmppConfiguration.h
    
    # Source files
    SOURCES += qxmpp/source/QXmppConfiguration.cpp

