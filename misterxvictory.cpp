#include "misterxvictory.h"
#include "ui_misterxvictory.h"

MisterxVictory::MisterxVictory(MisterxEngine *engine,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MisterxVictory)
{

    ui->setupUi(this);
}

MisterxVictory::~MisterxVictory()
{
    delete ui;
}

void MisterxVictory::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
