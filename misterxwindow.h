#ifndef MisterxWINDOW_H
#define MisterxWINDOW_H

#include <QMainWindow>
#include <QGraphicsItem>
#include <QGraphicsScene>
#include "misterxengine.h"

namespace Ui {
    class MisterxWindow;
}

class MisterxWindow : public QWidget {
    Q_OBJECT
public:
    MisterxWindow(MisterxEngine *engine, QWidget *parent = 0);
    ~MisterxWindow();
    QTimer* paintTimer;
public slots:
    void on_hostButton_clicked();
    void on_joinButton_clicked();
    void on_settingsButton_clicked();


signals:
    void hostClicked();
    void joinClicked();
    void settingsClicked();


protected:
    void changeEvent(QEvent *e);
    void paintEvent(QPaintEvent* event);

private:
    Ui::MisterxWindow *ui;
    MisterxEngine *engine;
    QTimer *timer;

    //////////////paint////////////
    QGraphicsScene *scene;
    long angle;
    QGraphicsRectItem *state;



private slots:




};

#endif // MisterxWINDOW_H
