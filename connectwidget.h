#ifndef CONNECTWIDGET_H
#define CONNECTWIDGET_H

#include <QtGui/QWidget>
#include "ui_connectwidget.h"
#include <QStringListModel>
#include "misterxengine.h"


namespace Ui
{
    class ConnectWidget;
}

class ConnectWidget : public QWidget
{
    Q_OBJECT

private:
    //QStringListModel *listmodel;

public:
    ConnectWidget(MisterxEngine *engine,QWidget *parent = 0);
    ~ConnectWidget();
    QList<QString> playerList;
    QList<QString> onlineList;

public slots:


signals:
    void startButtonClicked();
    void backButtonClicked();



private:
    Ui::ConnectWidget *ui;
    QStringList coord;
    QStringListModel * listmodel;
    QStringListModel * listmodel2;


private slots:
    void on_startButton_clicked();
    void on_backButton_clicked();




};

#endif // MAINWINDOW_H
