#include <QtGui/QApplication>
#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    #ifdef Q_OS_SYMBIAN
        w.showFullScreen();
    #else
        w.show();
    #endif
    w.show();
    return a.exec();
}
