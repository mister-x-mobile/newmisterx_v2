#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include "misterxengine.h"
#include "mapwidget.h"
#include "misterxsettingsform.h"
#include "misterxwindow.h"
#include "connectwidget.h"
#include "misterxvictory.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QWidget {
    Q_OBJECT
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::MainWindow *ui;
    MisterxEngine *engine;
    MapWidget *map;
    MisterxSettingsForm *settings;
    MisterxWindow *misterx;
    ConnectWidget *connection;
    MisterxVictory *victory;

    #ifdef Q_OS_SYMBIAN
        bool bDefaultIapSet;
    #endif

private slots:
    void showMap();
    void showDirectMap();
    void showSettings();
    void showMisterx();
    void showConnect();
    void showVictory();

};

#endif // MAINWINDOW_H
