#include "mainwindow.h"
#include "ui_mainwindow.h"

#ifdef Q_OS_SYMBIAN
#include "sym_iap_util.h"
#endif

MainWindow::MainWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MainWindow)
{
    QCoreApplication::setOrganizationName("MisterX");
    QCoreApplication::setOrganizationDomain("epfl.ch");
    QCoreApplication::setApplicationName("mister-x-mobile");
    ui->setupUi(this);

    #ifdef Q_OS_SYMBIAN
        // Set accespoint as default, don't ask again.
        qt_SetDefaultIap();
    #endif

    this->engine = new MisterxEngine(this);
    this->map = new MapWidget(this->engine, this);
    this->settings = new MisterxSettingsForm(this);
    this->misterx = new MisterxWindow(this->engine, this);
    this->connection = new ConnectWidget(this->engine,this);
    this->victory = new MisterxVictory(this->engine,this);

    // Default widget
    ui->stackedWidget->addWidget(this->map);
    ui->stackedWidget->addWidget(this->settings);
    ui->stackedWidget->addWidget(this->misterx);
    ui->stackedWidget->addWidget(this->connection);
    ui->stackedWidget->addWidget(this->victory);

    ui->stackedWidget->setCurrentWidget(this->misterx);

    connect(this->misterx, SIGNAL(hostClicked()), this, SLOT(showConnect()));
    connect(this->misterx, SIGNAL(joinClicked()), this, SLOT(showMap()));
    connect(this->misterx, SIGNAL(settingsClicked()), this, SLOT(showSettings()));

    connect(this->settings, SIGNAL(cancelClicked()),this,SLOT(showMisterx()));

    connect(this->map, SIGNAL(areaDefined()), this->engine, SLOT(startGame()));

    connect(this->engine, SIGNAL(gameStarted()), this->map, SLOT(initMarker()));
    connect(this->engine, SIGNAL(gameStarted()), this->map, SLOT(viewArea()));
    connect(this->engine, SIGNAL(gameUpdated(MisterxUser*)), this->map, SLOT(updateMarker(MisterxUser*)));
    connect(this->engine, SIGNAL(gameStoped()), this, SLOT(showVictory()));

    connect(this->connection, SIGNAL(backButtonClicked()),this,SLOT(showMisterx()));
    connect(this->connection, SIGNAL(startButtonClicked()),this,SLOT(showMap()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::showMap() {
    ui->stackedWidget->setCurrentWidget(this->map);
}

void MainWindow::showDirectMap() {
    this->map->defineArea();
    ui->stackedWidget->setCurrentWidget(this->map);
}

void MainWindow::showSettings() {
    ui->stackedWidget->setCurrentWidget(this->settings);
}

void MainWindow::showMisterx() {
    ui->stackedWidget->setCurrentWidget(this->misterx);
}

void MainWindow::showConnect() {
    ui->stackedWidget->setCurrentWidget(this->connection);
}

void MainWindow::showVictory() {
    ui->stackedWidget->setCurrentWidget(this->victory);
}
