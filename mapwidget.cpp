#include "mapwidget.h"
#include "ui_mapwidget.h"
#include <QWebFrame>
#include <QDebug>
#include <iostream>

MapWidget::MapWidget(MisterxEngine *engine, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MapWidget)
{
    ui->setupUi(this);
    QString mapHTML = "<html>"
                      "<head>"
                      "<meta name=\"viewport\" content=\"initial-scale=1.0, user-scalable=no\" />"
                      "<script type=\"text/javascript\" src=\"http://maps.google.com/maps/api/js?sensor=false\"></script>"
                      "<script type=\"text/javascript\">"
                        "var map;"
                        "var markers = {};"
                        "function initialize() {"
                        "var default_center = new google.maps.LatLng(46.51958, 6.63212);"
                        "var myOptions = {zoom: 13, center: default_center, disableDefaultUI: true, mapTypeId: google.maps.MapTypeId.ROADMAP};"
                        "map = new google.maps.Map(document.getElementById(\"map_canvas\"), myOptions);"
                        "}"
                      "</script>"
                      "</head>"
                      "<body onload=\"initialize()\" style=\"margin: 0px;\">"
                        "<div id=\"map_canvas\" style=\"width:100%; height:100%\"></div>"
                      "</body>"
                      "</html>";
    ui->webView->setHtml(mapHTML);
    connect(ui->button_viewArea, SIGNAL(clicked()), this, SLOT(viewArea()));
    connect(ui->button_defineArea, SIGNAL(clicked()), this, SLOT(defineArea()));
    connect(ui->button_viewUser, SIGNAL(clicked()), this, SLOT(viewUser()));
    connect(ui->button_right, SIGNAL(clicked()), this, SLOT(navigateRight()));
    connect(ui->button_left, SIGNAL(clicked()), this, SLOT(navigateLeft()));
    connect(ui->button_up, SIGNAL(clicked()), this, SLOT(navigateUp()));
    connect(ui->button_down, SIGNAL(clicked()), this, SLOT(navigateDown()));
    connect(ui->button_zoomPlus, SIGNAL(clicked()), this, SLOT(zoomPlus()));
    connect(ui->button_zoomMinus, SIGNAL(clicked()), this, SLOT(zoomMinus()));

    zoomLevel = 12;

    ui->button_viewArea->hide();
    ui->button_viewUser->hide();
    ui->button_defineArea->setFocus();

    this->definedAreaCenterLat=0.0;
    this->definedAreaCenterLng=0.0;


    this->engine = engine;
}

MapWidget::~MapWidget()
{
    delete ui;
}

void MapWidget::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MapWidget::viewArea() {

    ui->webView->page()->mainFrame()->evaluateJavaScript(QString("map.setZoom(%1);").arg(zoomLevel));
    QString commitString = QString("var definedAreaCenter = new google.maps.LatLng(%1, %2);")
                           .arg(definedAreaCenterLat).arg(definedAreaCenterLng);
    ui->webView->page()->mainFrame()->evaluateJavaScript(commitString);
    ui->webView->page()->mainFrame()->evaluateJavaScript("map.panTo(definedAreaCenter);");
    QString test = ui->webView->page()->mainFrame()->evaluateJavaScript("map.getCenter();").toString();
    qDebug() << commitString << endl;

    //setMarkerPosition(0, 46.51698, 6.64612);

}

void MapWidget::viewUser()
{
    //get actual coordinates form me-user and zoom to a street-view level to it
    foreach(MisterxUser *user, this->engine->userList)
    {
        if(user->itsMe)
        {
            QStringList zoomUser;
            zoomUser << QString("var userPosition = markers[\"%1\"].getPosition();").arg(user->jid);
            zoomUser << "map.panTo(userPosition);"
                << "map.setZoom(17);";
            ui->webView->page()->mainFrame()->evaluateJavaScript(zoomUser.join("\n"));
            break;
        }
    }


}

void MapWidget::defineArea() {
    ui->webView->page()->mainFrame()->evaluateJavaScript("global_area = map.getCenter();");
    zoomLevel = ui->webView->page()->mainFrame()->evaluateJavaScript("map.getZoom();").toInt();

    definedAreaCenterLat = ui->webView->page()->mainFrame()->evaluateJavaScript("map.getCenter().lat();").toDouble();
    definedAreaCenterLng = ui->webView->page()->mainFrame()->evaluateJavaScript("map.getCenter().lng();").toDouble();
    qDebug() << "Center: Lat:" << qSetRealNumberPrecision(15) << definedAreaCenterLat << "  Lng: " << definedAreaCenterLng << "  Zoom Level: " << zoomLevel << endl;

    ui->button_defineArea->hide();
    ui->button_viewArea->show();
    ui->button_viewUser->show();
    ui->button_down->hide();
    ui->button_left->hide();
    ui->button_right->hide();
    ui->button_up->hide();
    ui->button_zoomMinus->hide();
    ui->button_zoomPlus->hide();

    //setMarker(46.51958, 6.63212, MARKER_USER);
    //setMarker(46.51968, 6.63232, MARKER_MISTERX);
    //setMarker(46.51973, 6.66253, MARKER_OTHERHUNTERS);

    emit areaDefined();

}

void MapWidget::setMarker(QString markerID, double lat, double lng, int modus)
{
    QStringList marker;
    switch(modus)
    {
        case MARKER_MISTERX:
            marker << QString("var markerPosition = new google.maps.LatLng(%1, %2);").arg(lat).arg(lng);
            // image location, image size in pixels, image origin, image anchor
            marker << "var image_misterX = new google.maps.MarkerImage('http://documents.epfl.ch/users/p/pe/peric/www/misterXimages/markerX.png',"
                 "new google.maps.Size(20,34), new google.maps.Point(0,0), new google.maps.Point(10,34));"

                   << QString("markers[\"%1\"] = new google.maps.Marker({position: markerPosition, map: map, icon: image_misterX, zIndex: 1 });").arg(markerID);

            ui->webView->page()->mainFrame()->evaluateJavaScript(marker.join("\n"));
            break;


        case MARKER_USER:
            marker << QString("var markerPosition = new google.maps.LatLng(%1, %2);").arg(lat).arg(lng);
            // image location, image size in pixels, image origin, image anchor
            marker << "var image_user = new google.maps.MarkerImage('http://documents.epfl.ch/users/p/pe/peric/www/misterXimages/green-dot.png',"
              "new google.maps.Size(32,32), new google.maps.Point(0,0), new google.maps.Point(16,32));"

                 << QString("markers[\"%1\"] = new google.maps.Marker({position: markerPosition, map: map, icon: image_user, zIndex: 2 });").arg(markerID);

            ui->webView->page()->mainFrame()->evaluateJavaScript(marker.join("\n"));
            break;


        case MARKER_OTHERHUNTERS:
            // to make more than one; define 'var marker_counter' and set marker[marker_counter] in javascript
            // the amount of markers has also to be given to this function setMarkers()
            marker << QString("var markerPosition = new google.maps.LatLng(%1, %2);").arg(lat).arg(lng);
            // image location, image size in pixels, image origin, image anchor
            marker << "var image_user2 = new google.maps.MarkerImage('http://documents.epfl.ch/users/p/pe/peric/www/misterXimages/green_little.png',"
                "new google.maps.Size(12,20), new google.maps.Point(0,0), new google.maps.Point(6,20));"

                   << QString("markers[\"%1\"] = new google.maps.Marker({position: markerPosition, map: map, icon: image_user2, zIndex: 0 });").arg(markerID);

            ui->webView->page()->mainFrame()->evaluateJavaScript(marker.join("\n"));
            break;

      }
}

void MapWidget::setMarkerPosition(QString markerID, double newLat, double newLng)
{
    QString setMarkerPos = QString("markers[\"%1\"].setPosition(new google.maps.LatLng(%2, %3));").arg(markerID).arg(newLat).arg(newLng);

    ui->webView->page()->mainFrame()->evaluateJavaScript(setMarkerPos);
}

void MapWidget::initMarker()
{
    int modus=0;
    foreach(MisterxUser *user, this->engine->userList)
    {
        switch(user->role) {
            case MisterxUser::Misterx:
                modus=MARKER_MISTERX;
            break;
            case MisterxUser::Player:
            if( user->itsMe ) {
                modus = MARKER_USER;
            } else {
                modus = MARKER_OTHERHUNTERS;
            }
        }
        qDebug() << "initMarker modus: " << modus << endl;
        this->setMarker(user->jid, user->lat, user->lng, modus);
    }
}

void MapWidget::updateMarker(MisterxUser* user)
{
    this->setMarkerPosition(user->jid, user->lat, user->lng);
}

void MapWidget::navigateRight()
{
    ui->webView->page()->mainFrame()->evaluateJavaScript(QString("map.panBy(20,0);"));
}

void MapWidget::navigateLeft()
{
    ui->webView->page()->mainFrame()->evaluateJavaScript(QString("map.panBy(-20,0);"));
}

void MapWidget::navigateUp()
{
    ui->webView->page()->mainFrame()->evaluateJavaScript(QString("map.panBy(0,-20);"));
}

void MapWidget::navigateDown()
{
    ui->webView->page()->mainFrame()->evaluateJavaScript(QString("map.panBy(0,20);"));
}

void MapWidget::zoomPlus()
{
    QStringList zoom;
    zoom << "var actualZoomLevel = map.getZoom();"
         << "actualZoomLevel += 1;"
         << "map.setZoom(actualZoomLevel);";
    ui->webView->page()->mainFrame()->evaluateJavaScript(zoom.join("\n"));
}

void MapWidget::zoomMinus()
{
    QStringList zoom;
    zoom << "var actualZoomLevel = map.getZoom();"
         << "actualZoomLevel -= 1;"
         << "map.setZoom(actualZoomLevel);";
    ui->webView->page()->mainFrame()->evaluateJavaScript(zoom.join("\n"));

}


